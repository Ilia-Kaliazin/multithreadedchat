package com.epam.client.console;

public class Assay {
    private EventAssay eventAssay;
    private static Assay instance;

    private Assay(EventAssay eventAssay){
        this.eventAssay = eventAssay;
    }

    public static Assay getInstance(EventAssay eventAssay){
        if(instance == null) return new Assay(eventAssay);
        return instance;
    }

    /**
     *  Перебор команд.
     */
    public void parsingCommand(String string) {
        String[] tempStrings;
        tempStrings = string.split(" ");

        switch (tempStrings[0]) {
            case "help" :
                eventAssay.onCommandHelp();
                return;
            case "rename":
                eventAssay.onCommandReName(tempStrings);
                return;
            case "connect":
                eventAssay.onCommandConnect();
                return;
            case "disconnect":
                eventAssay.onCommandDisconnect();
                return;
            case "exit":
                eventAssay.onCommandExit();
                return;
        }
        eventAssay.onIncorrect("[Console] Incorrect command. (=_=)\n");
    }

    /**
     *  Отправка сообщения серверу.
     */
    public void sendMessage(String string){
        eventAssay.sendToServer(string);
    }
}
