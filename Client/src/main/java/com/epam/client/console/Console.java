package com.epam.client.console;

import com.epam.client.configuration.EventConfig;

import java.util.Scanner;

public class Console extends Thread implements EventConfig {
    public Assay assay;
    private Scanner scanner;
    // Instance.
    private static Console instance;

    private Console(Assay assay){
        this.assay = assay;
        this.scanner = new Scanner(System.in);
    }

    public static Console getInstance(Assay assay){
        if(instance == null) return new Console(assay);
        return instance;
    }

    /**
     *  Время задержки.
     */
    private void sleep(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Вывод на экран консоли.
     */
    public void print(String string){
        System.out.print(string);
    }

    /**
     *  Получение данных с консоли.
     */
    public String inputValue(String string){
        return (string == null) ? scanner.nextLine() : string;
    }

    /**
     *  Запуск потока консоли клиента.
     */
    @Override
    public void run() {
        while (!isInterrupted()){
            sleep();
            // print("Command: ");
            String tempString = inputValue(null);
            if(tempString.length() > 0) {
                if (tempString.charAt(0) == '/') assay.parsingCommand(tempString.substring(1));
                else assay.sendMessage(tempString);
            }
        }
    }

    /**
     *  Запрос данных у юзера.
     */
    @Override
    public String onNotFoundValue(String tag) {
        print(String.format("[Config] Give me value for tag - %s: ", tag));
        return inputValue(null);
    }

    /**
     *  Выключение консоли клиента.
     */
    public void shutdown(){
        scanner.close();
        this.interrupt();
    }
}
