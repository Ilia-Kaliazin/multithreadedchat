package com.epam.client.console;

public interface EventAssay {
    void onCommandHelp();
    void onCommandReName(String[] name);
    void onCommandConnect();
    void onCommandDisconnect();
    void onCommandExit();
    void onIncorrect(String string);
    void sendToServer(String message);
}
