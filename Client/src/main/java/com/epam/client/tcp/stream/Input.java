package com.epam.client.tcp.stream;

import java.io.BufferedReader;
import java.io.IOException;

public class Input extends Thread {
    EventStream eventStream;
    BufferedReader bufferedReader;

    public Input(EventStream eventStream, BufferedReader bufferedReader) {
        this.eventStream = eventStream;
        this.bufferedReader = bufferedReader;
        // Start.
        this.start();
    }

    /**
     *  Слушатель сообщений клиента.
     */
    @Override
    public void run() {
        try {
            while (!isInterrupted()) {
                eventStream.onReaderInput(bufferedReader.readLine());
            }
        } catch (Exception e) {
            eventStream.onExceptionStream(this, e);
        }
    }

    /**
     *  Закрытие потока чтения сообщений клиента.
     */
    public void close (){
        try {
            bufferedReader.close();
            this.interrupt();
        } catch (IOException e) {
            System.err.println("(InputReader : close) Ошибка закрытия потока: " + e.getLocalizedMessage());
            eventStream.onExceptionStream(this, e);
        }
    }
}
