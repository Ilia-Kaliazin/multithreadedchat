package com.epam.client.tcp;

public interface EventJoin {
    void onDisconnect();
    void onReader(String string);
    void onException(Compound compound, Exception e);
}
