package com.epam.client.tcp.stream;

public interface EventStream {
    void onReaderInput(String string);
    void onExceptionStream(Input input, Exception e);
    void onExceptionStream(Output output, Exception e);
}
