package com.epam.client.tcp;

import com.epam.client.tcp.stream.EventStream;
import com.epam.client.tcp.stream.Input;
import com.epam.client.tcp.stream.Output;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;

public class Compound implements EventStream {
    public Socket clientSocket;
    private EventJoin eventJoin;

    private Input inputStream;
    public Output outputStream;
    private static Compound instance;

    private Compound(String ip, short port, EventJoin eventJoin) {
        this.eventJoin = eventJoin;
        try {
            this.clientSocket = new Socket(ip, port);
        } catch (IOException e) {
            onDisconnect();
        }
        // Methods.
        onConnect();
    }

    public static Compound getInstance (String ip, short port, EventJoin eventJoin){
        if(instance == null) return new Compound(ip, port, eventJoin);
        return instance;
    }

    /**
     * Соединение с клиентом.
     */
    private void onConnect() {
        try{
            inputStream = new Input(this, new BufferedReader(new InputStreamReader(clientSocket.getInputStream())));
            outputStream = new Output(this, new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())));
        } catch (Exception e) {
            eventJoin.onException(this, new ConnectException("[Compound] Server is not available."));
        }
    }

    /**
     * Отключение клиента и закрытие всех потоков.
     */
    public void onDisconnect (){
        try{
            clientSocket.close();
            outputStream.close();
            inputStream.close();
        } catch (Exception ignore) {}
    }


    /**
     * Событие, при котором пришло сообщение.
     */
    @Override
    public void onReaderInput(String string) {
        eventJoin.onReader(string);
    }

    /**
     * Событие, при котором произошла ошибка.
     */
    @Override
    public void onExceptionStream(Input input, Exception e) {
        eventJoin.onDisconnect();
    }

    /**
     * Событие, при котором произошла ошибка.
     */
    @Override
    public void onExceptionStream(Output output, Exception e) {
        eventJoin.onDisconnect();
    }
}
