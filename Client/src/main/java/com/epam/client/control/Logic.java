package com.epam.client.control;

import com.epam.client.Core;
import com.epam.client.tcp.Compound;
import com.epam.client.tcp.EventJoin;
import com.epam.client.console.Console;
import com.epam.client.model.TicketUser;
import com.epam.client.console.EventAssay;
import com.epam.client.configuration.Config;

import java.net.Socket;

public class Logic implements EventAssay, EventJoin {
    private Core core;
    private Config config;
    private Console console;
    private Compound compound;
    private Socket compoundSocket;
    private TicketUser ticketUser;
    private ParserRequestServer parserRequestServer;
    // Instance
    private static Logic instance;

    private Logic(Core core){
        this.core = core;
        this.parserRequestServer = ParserRequestServer.getInstance(this);
    }

    public static Logic getInstance(Core core){
        if(instance == null) return new Logic(core);
        return instance;
    }

    /**
     *  Метод отправки данных на запросы сервера.
     */
    private void replyServer(String str1, String str2) {
        sendToServer(String.format(":%s:%s", str1, str2));
    }

    /**
     * Создание объекта подключения.
     */
    private void connectToServer(String ip, short port) {
        this.compound = Compound.getInstance(ip, port, this);
        this.compoundSocket = compound.clientSocket;
    }

    /**
     *  Отключение при встрече ошибок.
     */
    @Override
    public void onDisconnect() {
        if(compound != null) compound.onDisconnect();
        compoundSocket = null;
    }

    /**
     *  Чтение потока.
     */
    @Override
    public void onReader(String string) {
        if (string.charAt(0) == ':') parserRequestServer.run(string.substring(1));
        else console.print(string + "\n");
    }

    /**
     *  Обработчик ошибок.
     */
    @Override
    public void onException(Compound compound, Exception e) {
        console.print(String.format("%s\n[Exception] Connection is broken. :-O\n", e.getMessage()));
        onDisconnect();
    }

    /**
     *  Обработка команд.
     */

    @Override
    public void onCommandHelp() {
        console.print("[1] /help - list command.\n" +
                "[2] /rename (nick) - rename you nick.\n" +
                "[3] /connect - connect to server.\n" +
                "[4] /disconnect - exit server.\n" +
                "[5] /exit - exit program.\n");
    }

    @Override
    public void onCommandReName(String[] name) {
        if(name == null || name.length != 2 || name[1].equals("null")) {
            console.print("[Rename] Nickname incorrect. (+_+)\n");
            return;
        }
        ticketUser.setName(name[1]);
        console.print(String.format("[Rename] Your nickname - %s. (^_^)\n", ticketUser.getName()));
    }

    @Override
    public void onCommandConnect() {
        if(compoundSocket == null) {
            console.print("[Client] Connect from server. :-D\n");
            connectToServer(config.requestIp(), config.requestPort());
        }
        else console.print("[Compound] You are connected to server. (o_0)\n");
    }

    @Override
    public void onCommandDisconnect() {
        if(compoundSocket == null) {
            console.print("[Client] You are not connected to server. (+)_+)\n");
            return;
        }
        console.print("[Client] Disconnect from server. :-C\n");
        onDisconnect();
    }

    /**
     * Метод отправки сообщений.
     */
    @Override
    public void sendToServer(String string) {
        if(!compoundSocket.isClosed()) compound.outputStream.send(string);
    }

    /**
     *  Выход из приложения.
     */
    @Override
    public void onCommandExit() {
        console.print("Buy buy. ;-C\n");
        onDisconnect();
        core.shutdown();
    }

    /**
     *  Обработка некорректных действий пользователя.
     */
    @Override
    public void onIncorrect(String string) {
        console.print(string);
    }

    /**
     *  Обработка запросов от сервера.
     */
    public void onRequestServer(String string) {
        switch (string){
            case "setName" : replyServer(string, ticketUser.getName()); return;
        }
        onIncorrect("[Client] Server send an invalid request.\n");
    }

    /**
     *  Загрузка зависимостей.
     */
    public void setDependency (Console console, Config config, TicketUser ticketUser){
        this.config = config;
        this.console = console;
        this.ticketUser = ticketUser;
    }

    /**
     *  Отключение соединения.
     */
    public void shutDown() {
        onDisconnect();
    }
}
