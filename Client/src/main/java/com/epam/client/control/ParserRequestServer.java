package com.epam.client.control;

public class ParserRequestServer {
    private Logic logic;
    // Instance
    private static ParserRequestServer instance;

    private ParserRequestServer(Logic logic){
        this.logic = logic;
    }

    public static ParserRequestServer getInstance(Logic logic){
        if(instance == null) return new ParserRequestServer(logic);
        return instance;
    }

    /**
     *  Парсинг входящих запросов.
     */
    public void run(String string) {
        String[] tempStrings;
        tempStrings = string.split(":");

        switch (tempStrings[0]){
            case "getName" : logic.onRequestServer("setName"); return;
        }
        logic.onIncorrect("[Server] Incorrect request.\n");
    }
}
