package com.epam.client.ioexception;

import java.io.IOException;

public class InCorrectNameIOException extends IOException {
    public InCorrectNameIOException() {
    }

    public InCorrectNameIOException(String message) {
        super(message);
    }

    public InCorrectNameIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public InCorrectNameIOException(Throwable cause) {
        super(cause);
    }
}
