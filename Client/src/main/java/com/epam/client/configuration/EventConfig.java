package com.epam.client.configuration;

public interface EventConfig {
    String onNotFoundValue(String tag);
}
