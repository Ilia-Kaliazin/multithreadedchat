package com.epam.client.configuration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.io.FileInputStream;

public class Config {
    private File file;
    private Properties properties;
    private EventConfig eventConfig;
    private FileInputStream propFile;
    // Instance
    private static Config instance;

    private Config(EventConfig eventConfig) {
        this.eventConfig = eventConfig;
    }

    public static Config getInstance(EventConfig eventConfig){
        if(instance == null) return new Config(eventConfig);
        return instance;
    }

    /**
     * Получение статуса файла.
     */
    private boolean getStatusFile () {
        return file.isFile();
    }

    /**
     *  Загрузка файла в мапу.
     */
    private void loadFile() throws IOException {
        propFile = new FileInputStream(file);
        properties = new Properties(System.getProperties());
        properties.load(propFile);
    }

    /**
     *  Запись в файл новых параметров.
     */
    public void writeToFile(String tag, String string) {
        properties.setProperty(tag, string);
        try (FileWriter fileWriter = new FileWriter(file, true)) {
            fileWriter.write(String.format("%s=%s\n", tag, string));
            fileWriter.flush();
        } catch (IOException ignored) {}
    }

    /**
     *  Создание файла при его отсутствии.
     */
    private void verifyFile(){
        if(!getStatusFile()) {
            createNewFile();
        }
    }

    /**
     *  Создание файла.
     */
    public void createNewFile() {
        try {
            file.createNewFile();
        } catch (IOException ignored) { }
    }

    /**
     *  Запрос на получение порта из файла.
     */
    public short requestPort(){
        try {
            return Short.parseShort(getValue("PORT"));
        } catch (IOException e) {
            return 0;
        }
    }

    /**
     *  Запрос на получение ip аддреса из файла.
     */
    public String requestIp(){
        try {
            return getValue("IP");
        } catch (IOException e) {
            return null;
        }
    }

    /**
     *  Запрос по тэгу.
     */
    public String getValue(String tag) throws IOException {
        verifyFile();
        loadFile();

        String request = properties.getProperty(tag);
        if (request == null) {
            request = eventConfig.onNotFoundValue(tag);
            writeToFile(tag, request);
        }
        return request;
    }

    /**
     *  Указатель на нужный файл.
     */
    public void setFile(String string){
        file = new File(string);
    }

    /**
     *  Выключение.
     */
    public void shutdown() throws IOException {
        propFile.close();
    }
}
