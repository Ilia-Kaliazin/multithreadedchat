package com.epam.client;

import com.epam.client.console.Assay;
import com.epam.client.control.Logic;
import com.epam.client.console.Console;
import com.epam.client.model.TicketUser;
import com.epam.client.configuration.Config;

import java.io.IOException;

public class Core {
    private Logic logic;
    private Config config;
    private Console console;
    private TicketUser ticketUser;

    public Core() {
        this.logic = Logic.getInstance(this);
        Assay assay = Assay.getInstance(logic);
        this.console = Console.getInstance(assay);
        this.config = Config.getInstance(console);
        this.ticketUser = TicketUser.getInstance(console);
        // Upload.
        uploadDependencies();
        // Start.
        console.start();
    }

    /**
     *  Загрузка зависимостей.
     */
    private void uploadDependencies() {
        config.setFile("config.properties");
        logic.setDependency(console, config, ticketUser);
    }

    /**
     *  Выключение приложения.
     */
    public void shutdown() {
        try {
            config.shutdown();
            console.shutdown();
            logic.shutDown();
        } catch (IOException ignore) {}
    }
}
