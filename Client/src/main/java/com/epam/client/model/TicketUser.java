package com.epam.client.model;

import com.epam.client.console.Console;
import com.epam.client.ioexception.InCorrectNameIOException;

public class TicketUser {
    private String userName;
    private final int maxLength = 15;
    private Console console;
    // Instance
    private static TicketUser instance;

    private TicketUser(Console console){
        this.console = console;
        // Start.
        run();
    }

    public static TicketUser getInstance(Console console){
        if(instance == null) return new TicketUser(console);
        return instance;
    }

    /**
     *  Запрос имени у пользователя.
     */
    private void requestName() throws InCorrectNameIOException {
        console.print("Give me you name: ");
        if(setName(console.inputValue(null))) return;
        throw new InCorrectNameIOException("Your name is longer than allowed.");
    }

    /**
     *  Запуск сканера, для получения имени.
     */
    private void run(){
        while (true){
            try{
                requestName();
                break;
            } catch (InCorrectNameIOException e){
                console.print(e.getMessage());
            }
        }
    }

    public boolean setName(String string){
        if (string.length() > maxLength) return false;
        this.userName = string;
        return true;
    }

    public String getName() {
        return userName;
    }
}
