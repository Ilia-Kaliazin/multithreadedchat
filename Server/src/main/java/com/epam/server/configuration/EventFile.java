package com.epam.server.configuration;

import java.io.IOException;

public interface EventFile {
    void onIOException(IOException e);
}
