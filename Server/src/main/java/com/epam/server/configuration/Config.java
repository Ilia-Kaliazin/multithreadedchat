package com.epam.server.configuration;

import java.io.IOException;
import com.epam.server.control.console.Console;

public class Config implements EventFile {
    public File file;
    private Console console;
    private static Config instance;

    private Config (Console console){
        this.file = File.getInstance(this);
        this.console = console;
    }

    public static Config getInstance(Console console){
        if(instance == null) return new Config(console);
        return instance;
    }

    /**
     *  Запрос недостающих данных в config.properties
     */
    private String getValueForTag(String string){
        console.printInConsole("[Config properties] Give me value for tag - " + string + ": ");
        return console.inputValue(null);
    }

    /**
     *  Получение данных из файла.
     */
    public String getValue(String tag) {
        file.setFile("config.properties");
        if(file.getValue(tag) == null){
            file.writeToFile(tag, getValueForTag(tag));
        }
        return file.getValue(tag);
    }

    /**
     *  Получение данных из файла по тэгу LOG_DIR
     */
    public String getLog(){
        file.setFile(getValue("LOG_DIR"));
        return file.getValue();
    }

    /**
     *  Получение данных из файла по тэгу STORY_DIR
     */
    public String getStory(){
        file.setFile(getValue("STORY_DIR"));
        return file.getValue();
    }

    /**
     *  Получение данных из файла по тэгу AMOUNT_STORY
     */
    public String getAmountStory(){
        return getValue("AMOUNT_STORY");
    }

    /**
     * Сохранение данных в историю.
     */
    public void save (String string, String dir) {
        file.setFile(getValue(dir));
        file.writeToFile(string);
    }

    /**
     *  Обработчик ошибок.
     */
    @Override
    public void onIOException(IOException e) {
        System.err.println("(Config) error: " + e.getMessage());
    }
}
