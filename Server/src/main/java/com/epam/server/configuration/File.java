package com.epam.server.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Properties;

public class File {
    private Date date;
    private java.io.File file;
    private EventFile eventFile;
    private Properties properties;
    private FileInputStream propFile;
    // Instance
    private static File instance;

    private File(EventFile eventFile){
        date = new Date();
        this.eventFile = eventFile;
    }

    public static File getInstance(EventFile eventFile){
        if(instance == null) return new File(eventFile);
        return instance;
    }

    /**
     * Получение статуса файла.
     */
    private boolean getStatusFile () {
        return file.isFile();
    }

    /**
     *  Загрузка файла в мапу.
     */
    private void loadFile() {
        try {
            propFile = new FileInputStream(file);
            properties = new Properties(System.getProperties());
            properties.load(propFile);
        } catch (IOException ignored) {}
    }

    /**
     *  Создание файла при его отсутствии.
     */
    private boolean verifyFile(){
        if(!getStatusFile()) {
            createNewFile();
            return false;
        }
        return true;
    }

    /**
     *  Запись в файл, с указанием даты.
     */
    public void writeToFile(String string){
        writeToFile(String.format("%s", LocalDate.now()), string);
    }

    /**
     *  Запись в файл новых параметров.
     */
    public void writeToFile(String tag, String string) {
        properties.setProperty(tag, string);
        try (FileWriter fileWriter = new FileWriter(file, true)) {
            fileWriter.write(String.format("%s=%s\n", tag, string));
            fileWriter.flush();
        }catch (IOException e) {}
    }

    /**
     *  Создание файла.
     */
    public void createNewFile() {
        try {
            file.createNewFile();
        } catch (IOException ignored) {}
    }

    /**
     * Получение истории по дате.
     */
    public String getValue () {
        verifyFile();
        loadFile();
        return properties.getProperty(String.format("%s", LocalDate.now()));
    }

    /**
     *  Получение значений по тэгу.
     */
    public String getValue (String tag) {
        verifyFile();
        loadFile();
        return properties.getProperty(tag);
    }

    /**
     *  Указатель на нужный файл.
     */
    public void setFile(String string) {
        this.file = new java.io.File(string);
    }

    /**
     *  Выключение.
     */
    public void shutDown() throws IOException {
        propFile.close();
    }
}
