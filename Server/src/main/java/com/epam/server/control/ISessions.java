package com.epam.server.control;

import com.epam.server.tcp.Compound;

public interface ISessions {
    void verifyClient(Compound compound, String string);
    void onExceptionVerify(Compound compound);
}
