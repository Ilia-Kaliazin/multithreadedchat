package com.epam.server.control.console.commands;

public class CHelp {
    private static CHelp instance;

    public static CHelp getInstance(){
        if(instance == null) return new CHelp();
        return instance;
    }

    /*
    Список команд.
     */
    public String get(){
        return "[1] /help - List command.\n" +
                "[2] /log (name) - List connect log.\n" +
                "[3] /chat (count) - List message.\n" +
                "[4] /kick (user) - Kick user.\n" +
                "[5] /send (user) - Send message user.\n" +
                "[6] /shutdown - server shutdown.";
    }
}
