package com.epam.server.control.console.commands;

import com.epam.server.control.Sessions;
import com.epam.server.control.console.EventParser;
import com.epam.server.control.console.ParserCommand;

public class CKick {
    private Sessions sessions;
    private static CKick instance;
    private EventParser eventParser;

    private CKick(EventParser eventParser, Sessions sessions){
        this.sessions = sessions;
        this.eventParser = eventParser;
    }

    public static CKick getInstance(ParserCommand eventParser, Sessions sessions){
        if(instance == null) return new CKick(eventParser, sessions);
        return instance;
    }

    /*
    Провера на входное имя.
     */
    private boolean validateName(String[] string){
        if(string.length == 1) {
            eventParser.print("[CKick] User must have name.");
            return false;
        }
        return true;
    }

    /*
    Выполнение команды kick user.
     */
    public void run(String[] string){
        if(!validateName(string)) return;
        sessions.cKick(string[1]);
    }
}
