package com.epam.server.control;

import java.util.List;
import java.util.ArrayList;

public class Log {
    private String tempString;
    protected List<String> listLog = new ArrayList<>();
    // Instance
    private static Log instance;
    // Constructor
    private Log () {}

    public static Log getInstance(){
        if(instance == null) return new Log();
        return instance;
    }

    /**
     * Загрузка истории.
     */
    private void upLoadLog(String string){
        if(string == null) return;
        String[] strings = string.substring(1, string.length() - 1).split("\\|");
        for (String str : strings){
            listLog.add(str);
        }
    }

    /**
     *  Формирование строки вывода.
     */
    private void getList () {
        tempString = "[Log]";

        int countJoin = 0;
        int countDis = 0;

        for(String str : listLog) {
            tempString = String.format("%s\n%s", tempString, str);
            if(str.charAt(1) == '+') countJoin++;
            if(str.charAt(1) == '-') countDis++;
        }

        tempString = String.format("%s\n\n[Online: %s | Total: %s]", tempString, countJoin - countDis, countJoin);
    }

    /**
     * Сбор информации в переменную tempString по имени.
     */
    private void getByName (String string) {
        tempString = String.format("[Log by name: %s]", string);

        for(String str : listLog){
            if(str.contains(string)) tempString = String.format("%s\n%s", tempString, str);
        }
    }

    /**
     * Состояние листа.
     */
    private boolean getStatus(){
        return (listLog.size() > 0);
    }

    /**
     *  Получение списка логов.
     */
    public String get (String[] strings){
        if(!getStatus()) return "[Log] List clear.";
        if(strings.length == 2) getByName(strings[1]);
        else getList();

        return tempString;
    }

    /**
     *  Собирает всю информацию в логах и записывает в строку.
     */
    public String getString(){
        String outString = "{";
        for(String string : listLog){
            outString += string.replace("|", "") + "|";
        }
        return outString + "[Server] Shutdown. Save story.}";
    }

    /**
     * Добавить сообщение в лог.
     */
    public void addList (String string) {
        listLog.add(string);
    }

    /**
     *  Загрузка истории.
     */
    public void setStory(String string){
        upLoadLog(string);
    }
}
