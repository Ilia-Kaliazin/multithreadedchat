package com.epam.server.control.parser;

import com.epam.server.tcp.Compound;
import com.epam.server.control.ISessions;

public class ParserValue {
    private Compound compound;
    private String[] string;
    private ISessions iSessions;
    // Instance
    private static ParserValue instance;

    private ParserValue (ISessions iSessions){
        this.iSessions = iSessions;
    }

    public static ParserValue getInstance (ISessions iSessions){
        if(instance == null) return new ParserValue(iSessions);
        return instance;
    }

    /**
     *  Проверка массива на четность; [запрос:ответ]
     */
    private boolean verifyCorrectly() {
        if(string == null) return false;
        return ((string.length % 2) == 0);
    }

    /**
     * Верификация имени пользователя.
     */
    private void inputName(String string) {
        if(string == null || string.equals("null")) {
            iSessions.onExceptionVerify(compound);
            return;
        }
        if(compound.clientName != null) return;
        iSessions.verifyClient(compound, string);
    }

    /**
     * Список обрабатываемых тэгов(запросов).
     */
    private void listTag(String str1, String str2){
        switch (str1){
            case "setName" : inputName(str2); break;
        }
    }

    /**
     *  Дробление строки на части.
     */
    private void parsing(){
        for(int i = 0; i < string.length / 2; i = i + 2){
            listTag(string[i], string[i + 1]);
        }
    }

    /**
     *  Разделение строки по символу.
     */
    public void run(Compound compound, String string){
        this.compound = compound;
        this.string = string.split(":");

        if(!verifyCorrectly()) return;
        parsing();
    }
}
