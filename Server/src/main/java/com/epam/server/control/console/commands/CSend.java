package com.epam.server.control.console.commands;

import com.epam.server.control.Sessions;
import com.epam.server.control.console.EventParser;
import com.epam.server.control.console.ParserCommand;

public class CSend {
    private Sessions sessions;
    private static CSend instance;
    private EventParser eventParser;

    private CSend(EventParser eventParser, Sessions sessions){
        this.sessions = sessions;
        this.eventParser = eventParser;
    }

    public static CSend getInstance(ParserCommand eventParser, Sessions sessions){
        if(instance == null) return new CSend(eventParser, sessions);
        return instance;
    }
    /*
    Отправка сообщения указанному user`у.
     */
    public void run(String[] strings){
        if(strings.length == 3) sessions.cSend(strings[1], strings[2]);
    }
}
