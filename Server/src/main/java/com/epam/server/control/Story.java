package com.epam.server.control;

import java.util.List;
import java.util.ArrayList;

public class Story {
    private String tempString;
    protected List<String> listStory = new ArrayList<>();
    // Instance
    private static Story instance;

    private Story() {}

    public static Story getInstance() {
        if (instance == null) return new Story();
        return instance;
    }

    /**
     * Загрузка истории.
     */
    private void upLoadStory(String string){
        if(string == null) return;
        String[] strings = string.substring(1, string.length() - 1).split("\\|");
        for (String str : strings){
            listStory.add(str);
        }
    }

    /**
     * Проверка на корректность числа.
     */
    private void validateArgs(String string) {
        try {
            getList(Integer.parseInt(string));
        } catch (NumberFormatException e) { }
    }

    /**
     * Формирование строки вывода.
     */
    private void getList(int i) {
        if (listStory.size() < i) i = listStory.size();
        tempString = String.format("[Story] Last %d message.", i);

        for (String string : listStory.subList(listStory.size() - i, listStory.size())) {
            tempString = String.format("%s\n%s", tempString, string);
        }
    }

    /**
     * Состояние листа.
     */
    protected boolean getStatus() {
        return (listStory.size() > 0);
    }

    /**
     *  Получение списка логов.
     */
    public String get(String[] strings) {
        if (!getStatus()) return "[Story] List clear.";
        if (strings.length == 2) validateArgs(strings[1]);
        else getList(5);

        return tempString;
    }

    /**
     *  Собирает всю информацию в логах и записывает в строку.
     */
    public String getString(){
        String outString = "{";
        for(String string : listStory){
            outString += string.replace("|", "") + "|";
        }
        return outString + "[Server] Shutdown. Save story.}";
    }

    /**
     * Добавить сообщение в историю.
     */
    public void addList (String string) {
        listStory.add(string);
    }

    /**
     *  Загрузка истории.
     */
    public void setStory(String string){
        upLoadStory(string);
    }
}
