package com.epam.server.control;

import com.epam.server.tcp.Compound;
import com.epam.server.tcp.EventJoin;
import com.epam.server.control.parser.ParserValue;

import java.util.Date;
import java.util.List;
import java.net.Socket;
import java.util.ArrayList;


public class Sessions implements EventJoin, ISessions {
    // Храним все изменения связанные с подключением.
    private Log log;
    // Требуется для логов.
    private Date date;
    // Храним всю историю чата.
    private Story story;
    // Стандартное количество сообщений отправляемых клиенту при входе.
    private int amountStory = 5;
    // Парсер входных значений клиента.
    private ParserValue parserValue;
    private List<Compound> listClient = new ArrayList<>();
    // Instance
    private static Sessions instance;

    private Sessions(Log log, Story story){
        this.log = log;
        this.story = story;

        this.date = new Date();
        this.parserValue = ParserValue.getInstance(this);
    }

    public static Sessions getInstance(Log log, Story story) {
        if(instance == null) return new Sessions(log, story);
        return instance;
    }

    /*
        Отправка сообщения только одному клиенту.
     */
    private void sendClient(Compound compound, String string) {
        compound.outputStream.send(string);
    }

    /*
        Отправка сообщений всем клиентам сервера.
     */
    private void sendAll(String string) {
        story.listStory.add(string);

        for(Compound compound : listClient){
            if(!compound.clientSocket.isClosed()) sendClient(compound, string);
            else listClient.remove(compound);
        }
    }

    /*
        Отправка истории, при подключении на сервер.
     */
    private void sendStory(Compound compound) {
        if(!story.getStatus()) return;
        // Временная переменная хранящая количество выводимых сообщений.
        int j = amountStory;
        if(story.listStory.size() < j) j = story.listStory.size();
        // Цикл отправки сообщений.
        for(int i = 0; i < j; i++){
            sendClient(compound, story.listStory.get(story.listStory.size() - j + i));
        }
    }

    private Compound search (Socket socket) {
        for (Compound client : listClient){
            if(client.clientSocket.equals(socket)) return client;
        }
        return null;
    }

    private Compound search (String string) {
        for (Compound client : listClient){
            if(client.clientName.equals(string)) return client;
        }
        return null;
    }

    /*
        Обработка спецсимволов(тэгов) для получения информации о клиенте.
     */
    private void verifyTag (Compound compound, String string){
        parserValue.run(compound, string.substring(1));
    }

    /*
        Добавление клиента в лист соединений.
     */
    public void add (Compound client){
        listClient.add(client);
    }

    /*
        Удаление клиента. (Зная объект compound)
     */
    public void remove (Compound compound){
        listClient.remove(compound);
        if(compound.clientName == null || compound.clientName.equals("null")) return;
        log.listLog.add("[-] Disconnect " + compound.clientName + compound.clientSocket.getRemoteSocketAddress() + " [" + date.toString() + "]");
    }

    /*
        Удаление клиента. (Зная объект сокет)
     */
    public void remove (Socket socket) {
        listClient.remove(search(socket));
    }

    /*
        Количество сообщений истории, которые будут отправлять клиенту.
    */
    public void setAmount (String string) {
        try {
            this.amountStory = Integer.parseInt(string);
        } catch (NumberFormatException ignore) {}
    }

    /** Addon */
    public void cSend (String str1, String str2) {
        Compound client = search(str1);
        if(client == null) return;

        sendClient(client, String.format("[Console -> %s] %s", client.clientName, str2));
    }

    public void cKick (String string) {
        Compound client = search(string);
        if(client == null) return;

        sendAll("[Console] " + client.clientName + " kick.");
        onDisconnect(client);
    }
    /** \Addon */

    /*
        Присвоение персональных данных принятых от клиента.
     */
    @Override
    public void verifyClient(Compound compound, String string) {
        compound.clientName = string;
        onConnected(compound);
    }

    @Override
    public void onExceptionVerify(Compound compound) {
        sendClient(compound, "[Server] Rename you nick and reconnect server.");
        onDisconnect(compound);
    }

    /**
     * Обработка успешного подключения клиента с полученным именем.
     */
    @Override
    public void onConnected(Compound compound) {
        log.listLog.add("[+] Connect " + compound.clientName + compound.clientSocket.getRemoteSocketAddress() + " [" + date.toString() + "]");
        sendStory(compound);
        sendAll("[Join] " + compound.clientName + " connect.");
    }

    /**
     * При возникающих неполадках разрываем соединение.
     */
    @Override
    public void onDisconnect(Compound compound) {
        if(compound.clientSocket.isClosed()) return;
        // Remove user of list and disconnect.
        remove(compound);
        compound.onDisconnect();

        if(compound.clientName == null || compound.clientName.equals("null")) return;
        sendAll("[Done] " + compound.clientName + " disconnect.");
    }

    /**
     * Читаем поток клиента.
     * @param compound - клиент.
     * @param string - входная строка.
     */
    @Override
    public void onReader(Compound compound, String string) {
        if(string.charAt(0) == ':') {
            verifyTag(compound, string);
            return;
        }
        if(compound.clientName != null)
            sendAll("[Chat] " + compound.clientName + " : " + string);
    }

    /*
        Обработчик исключений клиента.
     */
    @Override
    public void onException(Compound compound, Exception e) {
        System.err.println("(Sessions : onException) " + e.getClass());
    }

    /*
        Разраваем связь со всеми клиентами.
    */
    public void shutDown() {
        while (listClient.size() > 0) {
            sendClient(listClient.get(0), "[Server] Server shutdown.");
            onDisconnect(listClient.get(0));
        }
    }
}
