package com.epam.server.control.console;

import com.epam.server.Core;
import com.epam.server.control.Log;
import com.epam.server.control.Story;
import com.epam.server.control.Sessions;

import java.util.Scanner;

public class Console extends Thread {
    private Core core;
    private Scanner input;
    private ParserCommand parserCommand;
    // Instance
    private static Console instance;

    private Console (Log log, Sessions sessions, Story story, Core core){
        input = new Scanner(System.in);

        this.core = core;
        this.parserCommand = ParserCommand.getInstance(this, log, sessions, story);
    }

    public static Console getInstance(Log log, Sessions sessions, Story story, Core core){
        if(instance == null) return new Console(log, sessions, story, core);
        return instance;
    }

    /**
     * Получение значений с консоли.
     */
    public String inputValue(String string){
        return (string == null) ? input.nextLine() : string;
    }

    /**
     *  Печать в консоль.
     */
    public void printInConsole(String string){
        System.out.print(string);
    }

    /**
     *  Выключение сервера.
     */
    public void shotDown(){
        core.shutDown();
    }

    /**
     *  Общение через консоль.
     */
    @Override
    public void run() {
        try{
            while (!isInterrupted()){
                printInConsole("Команда: ");
                parserCommand.run(inputValue(null).substring(1).split(" "));
            }
        } catch (Exception e){ }
    }
}
