package com.epam.server.control.console;

import com.epam.server.control.Log;
import com.epam.server.control.Story;
import com.epam.server.control.Sessions;
import com.epam.server.control.console.commands.CHelp;
import com.epam.server.control.console.commands.CKick;
import com.epam.server.control.console.commands.CSend;

public class ParserCommand implements EventParser {
    private Log log;
    private Story story;
    private Console console;
    private Sessions sessions;
    // Dependency
    private CHelp cHelp;
    private CKick cKick;
    private CSend cSend;
    // Instance
    private static ParserCommand instance;

    private ParserCommand(Console console, Log log, Sessions sessions, Story story){
        this.log = log;
        this.story= story;
        this.console = console;
        this.sessions = sessions;

        uploadDependency();
    }

    public static ParserCommand getInstance(Console console, Log log, Sessions sessions, Story story){
        if(instance == null) return new ParserCommand(console, log, sessions, story);
        return instance;
    }

    /**
     * Загрузка зависимостей(обработчиков команд)
     */
    private void uploadDependency(){
        this.cHelp = CHelp.getInstance();
        this.cKick = CKick.getInstance(this, sessions);
        this.cSend = CSend.getInstance(this, sessions);
    }

    /**
     *  Обработка текста с консоли.
     */
    public void run(String[] strings){
        switch (strings[0]){
            case "help" :   print(cHelp.get()); break;
            case "log" :    print(log.get(strings)); break;
            case "kick" :   cKick.run(strings); break;
            case "send" :   cSend.run(strings); break;
            case "chat" :   print(story.get(strings)); break;
            case "shutdown" : console.shotDown();
        }
    }

    /**
     * Вывод текста на консоль.
     */
    @Override
    public void print(String string) {
        console.printInConsole(string + "\n");
    }
}
