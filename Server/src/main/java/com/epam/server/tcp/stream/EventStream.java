package com.epam.server.tcp.stream;

public interface EventStream {
    void onReaderInput(String string);
    void onExceptionStream(Input input, Exception e);
    void onExceptionStream(Output output, Exception e);
}
