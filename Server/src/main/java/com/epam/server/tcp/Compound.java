package com.epam.server.tcp;

import com.epam.server.tcp.stream.Input;
import com.epam.server.tcp.stream.Output;
import com.epam.server.tcp.stream.EventStream;

import java.io.*;
import java.net.Socket;

public class Compound implements EventStream {
    public String clientName;
    public Socket clientSocket;
    private EventJoin eventJoin;

    private Input inputStream;
    public Output outputStream;

    public Compound(Socket clientSocket, EventJoin eventJoin) {
        this.eventJoin = eventJoin;
        this.clientSocket = clientSocket;
        // Methods.
        onConnect();
    }

    /**
     * Соединение с клиентом.
     */
    private void onConnect() {
        try{
            inputStream = new Input(this, new BufferedReader(new InputStreamReader(clientSocket.getInputStream())));
            outputStream = new Output(this, new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())));
            // Methods.
            requestInfo();
        } catch (Exception e) {
            System.err.println("(Compound : onConnect) Ошибка подключения: " + e.getLocalizedMessage());
            eventJoin.onException(this, e);
        }
    }

    /**
     * Запрос информации.
     */
    private void requestInfo(){
        outputStream.send(":getName");
    }

    /**
     * Отключение клиента и закрытие всех потоков.
     */
    public void onDisconnect (){
        try{
            clientSocket.close();
            inputStream.close();
            outputStream.close();
        } catch (Exception e){
            System.err.println("(Compound : onDisconnect) Ошибка отключения клиента: " + e.getLocalizedMessage());
            eventJoin.onException(this, e);
        }
    }


    /**
     * Событие, при котором пришло сообщение.
     */
    @Override
    public void onReaderInput(String string) {
        eventJoin.onReader(this, string);
    }

    /**
     * Событие, при котором произошла ошибка.
     */
    @Override
    public void onExceptionStream(Input input, Exception e) {
        eventJoin.onDisconnect(this);
    }

    /**
     * Событие, при котором произошла ошибка.
     */
    @Override
    public void onExceptionStream(Output output, Exception e) {
        eventJoin.onDisconnect(this);
    }
}
