package com.epam.server.tcp;

public interface EventJoin {
    void onConnected(Compound compound);
    void onDisconnect(Compound compound);
    void onReader(Compound compound, String string);
    void onException(Compound compound, Exception e);
}
