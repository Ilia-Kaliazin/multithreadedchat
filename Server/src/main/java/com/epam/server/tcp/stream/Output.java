package com.epam.server.tcp.stream;

import java.io.IOException;
import java.io.BufferedWriter;

public class Output {
    EventStream eventStream;
    BufferedWriter bufferedWriter;

    public Output (EventStream eventStream, BufferedWriter bufferedWriter) {
        this.eventStream = eventStream;
        this.bufferedWriter = bufferedWriter;
    }

    /**
     *  Отправка сообщения клиенту.
     */
    public synchronized void send (String string) {
        try {
            bufferedWriter.write(string + "\r\n");
            bufferedWriter.flush();
        } catch (IOException e) {
            eventStream.onExceptionStream(this, e);
        }
    }

    /**
     *  Закрытие потока отправки сообщений.
     */
    public void close (){
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            System.err.println("(OutputWriter : close) Ошибка закрытия потока: " + e.getLocalizedMessage());
            eventStream.onExceptionStream(this, e);
        }
    }
}
