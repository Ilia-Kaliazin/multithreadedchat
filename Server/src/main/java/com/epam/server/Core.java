package com.epam.server;

import com.epam.server.control.Log;
import com.epam.server.control.Story;
import com.epam.server.control.Sessions;
import com.epam.server.configuration.Config;
import com.epam.server.control.console.Console;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;

public class Core {
    private Log log;
    private Story story;
    private Listen listen;
    private Config config;
    private Console console;
    private Sessions sessions;
    private ServerSocket serverSocket;

    public Core() {
        run();
    }

    private boolean createServerSocket() throws IOException {
        try {
            serverSocket = new ServerSocket(Short.parseShort(config.getValue("PORT")));
            return true;
        } catch (BindException e) {
            console.printInConsole("[Server] Port is busy. (+_+)\n");
            return false;
        }
    }

    private void run() {
        System.out.println("Запускаю сервер..");
        try {
            log = Log.getInstance();
            story = Story.getInstance();
            sessions = Sessions.getInstance(log, story);
            console = Console.getInstance(log, sessions, story, this);
            config = Config.getInstance(console);
            // Create socket.
            if(!createServerSocket()) return;
            listen = Listen.getInstance(serverSocket, sessions);
            // Upload dependency.
            log.setStory(config.getLog());
            story.setStory(config.getStory());
            sessions.setAmount(config.getAmountStory());
            // Start.
            listen.start();
            console.start();
            // Started.
            log.addList("[Server] Server started.");
            story.addList("[Server] Server started.");
        } catch (IOException e) {
            System.err.println("[Server] Core: Критическая ошибка!");
            shutDown();
        }
    }

    /**
     * Выключение сервера с закрытием всех потоков.
     */
    public void shutDown() {
        try {
            console.printInConsole("[Server] ShotDown..\n");
            sessions.shutDown();
            listen.close();
            console.interrupt();
            console.printInConsole("[Server] Save config, log, story.\n");
            config.save(log.getString(), "LOG_DIR");
            config.save(story.getString(), "STORY_DIR");
            config.file.shutDown();
            console.printInConsole("[Server] Done.\n");
        } catch (IOException ignore) {}
    }
}
