package com.epam.server;

import com.epam.server.tcp.Compound;
import com.epam.server.control.Sessions;

import java.net.Socket;
import java.io.IOException;
import java.net.ServerSocket;

public class Listen extends Thread {
    private Sessions sessions;
    private Socket clientSocket;
    private ServerSocket serverSocket;
    // Instance
    private static Listen instance;

    private Listen(ServerSocket serverSocket, Sessions sessions){
        this.sessions = sessions;
        this.serverSocket = serverSocket;
    }

    public static Listen getInstance(ServerSocket serverSocket, Sessions sessions) {
        if(instance == null) return new Listen(serverSocket, sessions);
        return instance;
    }

    /**
     * Слушатель соединений.
     */
    @Override
    public void run() {
        try {
            while (!isInterrupted()){
                clientSocket = serverSocket.accept();
                sessions.add(new Compound(clientSocket, sessions));
            }
        } catch (IOException e){
            try {
                sessions.remove(clientSocket);
            } catch (Exception ignore) {}
        }
    }

    /**
     *  Закрытые слушателя.
     */
    public void close(){
        try {
            this.interrupt();
            serverSocket.close();
        } catch (IOException ignore) {}
    }
}
